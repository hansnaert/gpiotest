using System;
using System.Threading;
using RaspberryGPIOManager;

namespace GpioTest
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			Console.WriteLine ("Hello World!");
			GPIOPinDriver led1 = new GPIOPinDriver (GPIOPinDriver.Pin.GPIO2);
			led1.Direction = GPIOPinDriver.GPIODirection.Out;
			led1.State = GPIOPinDriver.GPIOState.High;
			for (int i=0; i<10; i++) {
				led1.State = led1.State == GPIOPinDriver.GPIOState.High ? GPIOPinDriver.GPIOState.Low : GPIOPinDriver.GPIOState.High;
				Thread.Sleep (1000);
			}
		}
	}
}

